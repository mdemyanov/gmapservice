
/*
 * GET home page.
 */

exports.index = function(req, res){
  res.render('index', { title: 'Express' });
};
exports.param = function(req, res){
    res.render('index', { title: req.param('searchBy') + req.param('uuid')});
};
exports.gmapSrv = function(req, res){
    res.render('gmapSrv', { title: 'gmapSrv' });
};
exports.gmapSrvByEmployees = function(req, res){
    res.render('gmapSrvByEmployees', {searchBy : req.param('searchBy'), uuid : req.param('uuid')});
};
exports.justMap = function(req, res){
    res.render('justMap', { title: 'gmapSrv' });
};